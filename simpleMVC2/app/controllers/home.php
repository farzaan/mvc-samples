<?php


class Home extends Controller{// 9-example url to call this controller : localhost/MVCexamples/simpleMVC2/public/home/index/var1/var2

    function __construct()
    {
        echo "hi from Home constructor";
    }

    public function index($var1, $var2){ // $defaultMethod in each controller for cases that no methode calls through url
        //echo $var1 . " " . $var2;
        $this->viewInclude("firstpage/index",['name'=>$var1,'age'=>$var2]);//vars will be included to the defined view!!!!
    }

}