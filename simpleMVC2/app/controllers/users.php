<?php

//****notice= no data control applied for simplicity

class Users extends Controller{
    // 9-example url to call this controller : localhost/MVCexamples/simpleMVC2/public/users/index
    // 9-example url to call this controller : localhost/MVCexamples/simpleMVC2/public/users/all
    // 9-example url to call this controller : localhost/MVCexamples/simpleMVC2/public/users/callById/1
    // 9-example url to call this controller : localhost/MVCexamples/simpleMVC2/public/users/callByName/farzaan1


    public function index(){
        echo "hello from users controller";
    }

    public function all(){
        $user = $this->modelInclude('User');
        print_r($user->getUsers());
    }

    public function callById($id){// 10-when we calling this method, we should send an integer parameter of user id
        $user = $this->modelInclude('User');// 11- call a method from parent core/Controller class to include model and get data from database
        $user->id = $id;//15-the $id property of User class set to id given from url (here $user will be an object of User class->see 14)

        $finalUser =$user->getUsersById(); // 16-we call getUserById() method from User Model
        $this->viewInclude("users/index",['id'=>$finalUser[0]['ID'],'username'=>$finalUser[0]['USERNAME']]);
        //17-we finally call needed View and pass parameters given from db to it (with viewInclude() method of parent core/Controller class)

    }

    public function callByName($name){
        $user = $this->modelInclude('User');
        $user->name = $name;
        $finalUser =$user->getUsersByName();
        $this->viewInclude("users/index",['id'=>$finalUser[0]['ID'],'username'=>$finalUser[0]['USERNAME']]);//vars will be included to the defined view!!!!
    }

}//19-return to core/App