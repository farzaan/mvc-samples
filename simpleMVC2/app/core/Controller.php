<?php


class Controller{ //our controllers core

    public function modelInclude($model){

        require_once "../app/models/{$model}.php";//12- Model will be included
        return new $model();//14- return an object of User model to controllers/users.php
    }

    public function viewInclude($view, $param1=[]){
        //18-view will be included and sent parameters ( $param1[] ) will be accessible within the view (because we required view inside this function)

        require_once "../app/views/{$view}.php";
        //return to controllers/users.php
    }

}