<?php

// example: test.com/public/home/index/param1/param2 ... -> we want to send needed controller
// and method and required parameters through Url in different oarts of our app
// example: controller: home / method: index / params: param1, param2 ...

// to access our controllers and methods through url


class App{ //our application core

    protected $defaultController = 'home';// if we have no included Controller and method in Url, these defaults will be called...
    protected $defaultMethod = 'index';
    protected $params = [];

    function __construct() // 3-when we instanciate App class this function will run
    {
        $url = $this->parseUrl(); // 4-call parseUrl to $_GET[] parameters from url and sanitize url too.
        //print_r($url);

        if(file_exists("../app/controllers/" . $url[0] . ".php")){ // 5-first parameter after root directory in url will be our needed Controller
            $this->defaultController = $url[0];
            unset($url[0]);
        }
        require_once "../app/controllers/{$this->defaultController}.php";

        $this->defaultController = new $this->defaultController; //we make an object from our controller to pass it to method_exist() function

        if(isset($url[1])) {
            if (method_exists($this->defaultController, $url[1])) {// 6-second parameter after root directory in url will be our needed method
                $this->defaultMethod = $url[1];
                unset($url[1]);
            }
        }

        $this->params = $url ? array_values($url) : [];// 7-all other parts in url will be parameters that should be send to method

        //print_r($this->params);

        call_user_func_array(array($this->defaultController, $this->defaultMethod), $this->params);// 8-now we call method and send parameters to it by call_user_func_array() function
    }

    function parseUrl(){
        if(isset($_GET['url'])) { //to get url's different parts seperately
            return explode('/', filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_URL));
        }
    }

}//20-return to public/index.php