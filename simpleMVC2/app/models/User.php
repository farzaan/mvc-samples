<?php


class User{//13- User model to get users information from Users table of db

    public $name = '';
    public $id = 0;

    const host = "localhost";
    const database = "mytestdb";
    const username = "root";
    const password = "";
    private $connection= "";

    private function connect()
    {


        try {
            $attributes = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8" //for farsi data
            );
            $this->connection = new PDO("mysql:host=" . SELF::host .
                ";dbname=" . SELF::database, SELF::username, SELF::password, $attributes);
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
        return $this->connection;

    }

    public function query($query, $params = array()){
        $statement = $this->connect()->prepare($query);
        $statement->execute($params);

        if(explode(' ',$query)[0]=='SELECT'){
            $data = $statement->fetchAll(2);
            return $data;
        }
    }

    public function getUsers(){
        return self::query("SELECT * FROM Users");
    }

    public function getUsersById(){
        return self::query("SELECT * FROM Users WHERE ID = ?",[$this->id]);
    }

    public function getUsersByName(){
        return self::query("SELECT * FROM Users where USERNAME = ?", [$this->name]);
    }

}

//return to core/Controller