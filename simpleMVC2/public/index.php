<?php

/*
 * as defined in .htaccess, every url that user enters will redirect to index first.
 * from url we $_GET a variable called "url" (due to .htaccess file)
*/



/*
 * if we dont want to get controller and method names from url using $_GET[], we can use $_SERVER['REQUEST_URI']
 * to get full url string (parts after root directory) and after that is same as before (in this case .htaccess file
 * will be more simple -> RewriteRule ^ index.php [QSA,L])
 */

require_once '../app/init.php'; //0-we have an init.php file to include all needed files

$app = new App; //2- make new instance of core/App class which is our application's main class

//21-finally we are here again :)