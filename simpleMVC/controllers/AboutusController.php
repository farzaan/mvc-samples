<?php

/*
 * 8-controller will decide what should we do when user set: sample.com/about-us
 */

class AboutusController extends Controller{// 9-called methodes of this class and parent class (Controller) will run.

    public static function doSomething(){ // 9-1- this function connects to Users model to gat users data from database and print it
        print_r(self::getUsers());
    }


}//10-now we return to Routes.php