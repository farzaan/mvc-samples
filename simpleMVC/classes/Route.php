<?php

/*
 * 6- here we compare user entered url ($_GET['url']) with our valid urls.
 */


class Route{

    public static $validRoutes=array();

    public static function set($route, $function){
        // 6-1- set() function will save our defined urls as valid urls and invoke $function in each call.
        self::$validRoutes[] = $route;
        //print_r(self::$validRoutes);

        if ($_GET['url']==$route) {
            $function->__invoke();
        }
    }


} // 7-we now return to Routes.php