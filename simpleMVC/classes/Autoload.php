<?php

// 2-here we autoload classes (to prevent including each class seperately in each file)
function __autoload($className){
    if(file_exists("classes/{$className}.php"))
    {
        // 2-1- autoload classes
        require_once "classes/{$className}.php";
    }
    elseif (file_exists("controllers/{$className}.php"))
    {
        // 2-2- autoload controllers (they are class too!)
        require_once "controllers/{$className}.php";
    }
    elseif (file_exists("model/{$className}.php"))
    {
        // 2-2- autoload Models (they are class too!)
        require_once "model/{$className}.php";
    }
    else
    {
        echo "not such a file or directory!";
    }
} // 3-now we return to index.php