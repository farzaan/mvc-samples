<?php
/**
 * by: farzaan
 * Date: 6/7/2020
 */

/*
 * as defined in .htaccess, every url that user enters will redirect to index first.
 * from url we $_GET a variable called "url" (due to .htaccess file)
*/


require_once('classes/Autoload.php'); // 1-first Autoload.php file will be loaded.
require_once('Routes.php'); // 4-we load Routes.php to decide where should we send user according to his/her entered url


// 12-finally we are here again ;)
