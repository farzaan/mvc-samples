<?php

/*
 * 9-1- This model has responsibility to connect to Users table of database
 */


class Users
{
    public static $host = "localhost";
    public static $database = "mytestdb";
    public static $username = "root";
    public static $password = "";

    public static function connect()
    {
        $pdo = new PDO("mysql:host=" . self::$host . ";dbname=" . self::$database . ";charset=utf8", self::$username, self::$password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;

    }

    public static function query($query, $params = array()){
        $statement = self::connect()->prepare($query);
        $statement->execute($params);

        if(explode(' ',$query)[0]=='SELECT'){
            $data = $statement->fetchAll(2);
            return $data;
        }
    }

    public static function getUsers(){
        return self::query("SELECT * FROM Users");
    }

}// 8-2- now we return to AboutusController.php