<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 6/7/2020
 * Time: 2:03 PM
 */

/*
 * 5-here we should check url entered by user and compare it to those that we defined
 *
 * to do so we defined a Route class with static set() methode and call it for each of our defined urls.
 * example: sample.com/about-us -> sample.com?url=about-us (due to .htaccess)
*/

Route::set('index.php',function () {// 6-go to Route.php
    // 8- createView() methode of AboutusController called to do what should we do when user set: sample.com/about-us
    // this view will call only for defined urls (because we check condition in set() in Route class)
    IndexController::createView('IndexView');
});

Route::set('about-us',function () {// 6-go to Route.php
    // 8- createView() methode of AboutusController called to do what should we do when user set: sample.com/about-us
    AboutusController::createView('AboutUsView');
    AboutusController::doSomething(); // 8-1- other methodes from AboutusController can be called.
});

Route::set('contact-us',function (){// 6-go to Route.php
    // 8- createView() methode of ContactusController called to do what should we do when user set: sample.com/contact-us
    ContactusController::createView('ContactUsView');
});

// 11-now we return to index.php